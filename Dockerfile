# Используем официальный образ Node.js в качестве базового
FROM node:14

# Создаем рабочую директорию
WORKDIR /app

# Копируем package.json и package-lock.json
COPY package*.json ./

# Устанавливаем зависимости
RUN npm install

# Копируем исходный код
COPY . .

# Открываем порт для приложения
EXPOSE 8080

# Запускаем приложение
CMD ["node", "app.js"]
