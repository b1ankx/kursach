### README.md

# Развертывание Node.js приложения с использованием Docker, Ansible и GitLab CI/CD

Этот проект демонстрирует полную CI/CD цепочку для Node.js приложения с использованием Docker, Ansible и GitLab CI/CD. Приложение разворачивается на сервере с Debian 12.

## Структура проекта

```yaml
├── ansible
│   ├── ansible.cfg
│   ├── hosts
│   └── playbook.yml
├── app.js
├── certs
│   ├── server.cert
│   └── server.key
├── Dockerfile
├── package.json
├── package-lock.json
├── README.md
└── views
    └── index.ejs
```

### Описание директорий

- **ansible/**: Содержит конфигурационные файлы и плейбуки Ansible.
  - `ansible.cfg`: Конфигурационный файл Ansible.
  - `hosts`: Инвентори файл, указывающий данные сервера.
  - `playbook.yml`: Ansible плейбук для развертывания Docker контейнера.

- **certs/**: Содержит SSL сертификаты.
  - `server.cert`: Сертификат сервера.
  - `server.key`: Ключ сервера.

- **views/**: Содержит шаблоны для рендеринга.
  - `index.ejs`: Главная страница.

- **app.js**: Основной файл Node.js приложения.
- **Dockerfile**: Файл для создания Docker образа приложения.
- **package.json**: Файл зависимостей Node.js.
- **package-lock.json**: Автоматически сгенерированный файл для точного определения версий зависимостей.

## Требования

- Docker
- Ansible
- GitLab CI/CD

## Настройка и развертывание

### 1. Настройка Ansible

#### ansible/ansible.cfg
```ini
[defaults]
inventory = ./hosts
remote_user = your_user
private_key_file = /path/to/your/private/key
host_key_checking = False
```

#### ansible/hosts
```ini
[web]
192.168.100.156
```

#### ansible/playbook.yml
```yaml
---
- hosts: web
  become: yes
  tasks:
    - name: Ensure Docker is installed
      apt:
        name: docker.io
        state: present
        update_cache: yes

    - name: Docker login
      shell: echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
      environment:
        CI_REGISTRY_USER: "{{ lookup('env', 'CI_REGISTRY_USER') }}"
        CI_REGISTRY_PASSWORD: "{{ lookup('env', 'CI_REGISTRY_PASSWORD') }}"
        CI_REGISTRY: "{{ lookup('env', 'CI_REGISTRY') }}"
      ignore_errors: yes

    - name: Pull latest Docker image
      shell: docker pull "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_SLUG"
      environment:
        CI_REGISTRY_USER: "{{ lookup('env', 'CI_REGISTRY_USER') }}"
        CI_REGISTRY_PASSWORD: "{{ lookup('env', 'CI_REGISTRY_PASSWORD') }}"
        CI_REGISTRY: "{{ lookup('env', 'CI_REGISTRY') }}"
        CI_PROJECT_NAMESPACE: "{{ lookup('env', 'CI_PROJECT_NAMESPACE') }}"
        CI_PROJECT_NAME: "{{ lookup('env', 'CI_PROJECT_NAME') }}"
        CI_COMMIT_REF_SLUG: "{{ lookup('env', 'CI_COMMIT_REF_SLUG') }}"

    - name: Run Docker container
      shell: docker run -d --name my_flask_app -p 80:5000 "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_SLUG"
      environment:
        CI_REGISTRY_USER: "{{ lookup('env', 'CI_REGISTRY_USER') }}"
        CI_REGISTRY_PASSWORD: "{{ lookup('env', 'CI_REGISTRY_PASSWORD') }}"
        CI_REGISTRY: "{{ lookup('env', 'CI_REGISTRY') }}"
        CI_PROJECT_NAMESPACE: "{{ lookup('env', 'CI_PROJECT_NAMESPACE') }}"
        CI_PROJECT_NAME: "{{ lookup('env', 'CI_PROJECT_NAME') }}"
        CI_COMMIT_REF_SLUG: "{{ lookup('env', 'CI_COMMIT_REF_SLUG') }}"
```

### 2. Настройка GitLab CI/CD

#### .gitlab-ci.yml
```yaml
stages:
  - build
  - deploy

build:
  stage: build
  script:
    - docker build -t "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_SLUG" .
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - docker push "$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME:$CI_COMMIT_REF_SLUG"

deploy:
  stage: deploy
  script:
    - apt-get update -qq
    - apt-get install -qq -y ansible
    - ansible-playbook -i ansible/hosts ansible/playbook.yml
  only:
    - main
```

## Переменные окружения GitLab CI/CD

Убедитесь, что следующие переменные окружения настроены в настройках проекта GitLab:

- `CI_REGISTRY`
- `CI_REGISTRY_USER`
- `CI_REGISTRY_PASSWORD`
- `CI_PROJECT_NAMESPACE`
- `CI_PROJECT_NAME`
- `CI_COMMIT_REF_SLUG`


